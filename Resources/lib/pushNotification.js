exports.pushNotification = function(){
	
	////////// PUSH NOTIFICATIONS /////////////

	if(TiGlobals.osname === 'android')
	{
		var deviceToken;
		var Cloud = require('ti.cloud');
		Cloud.debug = true;
		
		var CloudPush = require('ti.cloudpush');
		CloudPush.debug = true;
		CloudPush.enabled = true;
		CloudPush.showTrayNotificationsWhenFocused = false;
		CloudPush.focusAppOnPush = false;
		
		function checkDeviceTokenAndroid()
		{
			var xmlHttp = Ti.Network.createHTTPClient();
				
			xmlHttp.onload = function()
			{
				var result = JSON.parse(xmlHttp.responseText);
				require('/utils/Console').info(xmlHttp.responseText);
				if(result.push.error === 0)
				{
					// Not found in database
					
					Cloud.Users.create({
					    username:deviceToken.substr(0,90),
					    first_name: deviceToken.substr(0,90),
					    last_name: deviceToken.substr(0,90),
					    password: deviceToken.substr(0,90),
					    password_confirmation: deviceToken.substr(0,90)
					}, function (e) {
					    if (e.success) {
					        var user = e.users[0];
					        require('/utils/Console').info('Success:\n' +
					            'id: ' + user.id + '\n' +
					            'sessionId: ' + Cloud.sessionId + '\n' +
					            'first name: ' + user.first_name + '\n' +
					            'last name: ' + user.last_name);
					           
					        var xmlHttpACS = Ti.Network.createHTTPClient();
				
							xmlHttpACS.onload = function()
							{
								var resultACS = JSON.parse(xmlHttpACS.responseText);
								require('/utils/Console').info(xmlHttpACS.responseText);
								if(resultACS.push.error === 0)
								{
									loginAndroidCloudUser();
								}
								xmlHttpACS = null;
							};
						
							xmlHttpACS.onerror = function(e)
							{
								xmlHttpACS = null;
							};
							
							require('/utils/Console').info(TiGlobals.appURL + 'push/push.php?deviceid='+deviceToken+'&acsuser='+deviceToken.substr(0,90)+'&acspwd='+deviceToken.substr(0,90)+'&platform=android&acsid='+user.id);
							xmlHttpACS.open('POST', TiGlobals.appURL + 'push/push.php?deviceid='+deviceToken+'&acsuser='+deviceToken.substr(0,90)+'&acspwd='+deviceToken.substr(0,90)+'&platform=android&acsid='+user.id,true);
							xmlHttpACS.send({user:Ti.App.Properties.getString('userId')});
					           
					    } else {
					        require('/utils/Console').info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					    }
					});
				}
				else
				{
					loginAndroidCloudUser();
				}
				
				xmlHttp = null;
			};
		
			xmlHttp.onerror = function(e)
			{
				xmlHttp = null;
			};
			
			require('/utils/Console').info(TiGlobals.appURL + 'push/checkpush.php?deviceid='+deviceToken+'&platform=android');
			xmlHttp.open('POST', TiGlobals.appURL + 'push/checkpush.php?deviceid='+deviceToken+'&platform=android',true);
			xmlHttp.send({user:Ti.App.Properties.getString('userId')});
		}
		
		// Fetch device token
		CloudPush.retrieveDeviceToken({
			success : function deviceTokenSuccess(e) {
				deviceToken = e.deviceToken;
				Ti.App.Properties.setString('deviceToken',deviceToken);
				require('/utils/Console').info('Device Token: ' + deviceToken);
				
				// Check if this exists in database
				checkDeviceTokenAndroid();
			},
			error : function deviceTokenError(e) {
				require('/utils/Console').info('Failed to register for push! ' + e.error);
			}
		});
		
		function loginAndroidCloudUser(e) {
			//Create a Default User in Cloud Console, and login with same credential
			Cloud.Users.login({
				login : deviceToken.substr(0,90),
				password : deviceToken.substr(0,90)
			}, function(e) {
				if (e.success) {
					require('/utils/Console').info("Android Login success");
					androidCloudUserSubscribe();
				} else {
					require('/utils/Console').info('Android Login error: ' + ((e.error && e.message) || JSON.stringify(e)));
				}
			});
		}
		
		// Subscribe for push notification on cloud server
		function androidCloudUserSubscribe() {
			Cloud.PushNotifications.subscribe({
				channel : 'iConcerts',
				device_token : deviceToken,
				type : 'gcm'
			}, function(e) {
				if (e.success) {
					require('/utils/Console').info('Subscribed for Push Notification!');
				} else {
					require('/utils/Console').info('Subscribe error:' + ((e.error && e.message) || JSON.stringify(e)));
				}
			});
		}
		
		
		CloudPush.addEventListener('callback', function(evt) {
			require('/utils/Console').info(evt.payload);
		});
		CloudPush.addEventListener('trayClickLaunchedApp', function(evt) {
			require('/utils/Console').info('@@## Tray Click Launched App (app was not running)');
		});
		CloudPush.addEventListener('trayClickFocusedApp', function(evt) {
			require('/utils/Console').info('@@## Tray Click Focused App (app was already running)');
		});
	}
	else
	{
		var Cloud = require('ti.cloud');
		var deviceToken;
		
		function checkDeviceTokenIOS()
		{
			var xmlHttp = Ti.Network.createHTTPClient();
				
			xmlHttp.onload = function()
			{
				var result = JSON.parse(xmlHttp.responseText);
				require('/utils/Console').info(xmlHttp.responseText);
				if(result.push.error === 0)
				{
					// Not found in database
					
					Cloud.Users.create({
					    username:deviceToken.substr(0,90),
					    first_name: deviceToken.substr(0,90),
					    last_name: deviceToken.substr(0,90),
					    password: deviceToken.substr(0,90),
					    password_confirmation: deviceToken.substr(0,90)
					}, function (e) {
					    if (e.success) {
					        var user = e.users[0];
					        require('/utils/Console').info('Success:\n' +
					            'id: ' + user.id + '\n' +
					            'sessionId: ' + Cloud.sessionId + '\n' +
					            'first name: ' + user.first_name + '\n' +
					            'last name: ' + user.last_name);
					           
					        var xmlHttpACS = Ti.Network.createHTTPClient();
				
							xmlHttpACS.onload = function()
							{
								var resultACS = JSON.parse(xmlHttpACS.responseText);
								require('/utils/Console').info(xmlHttpACS.responseText);
								if(resultACS.push.error === 0)
								{
									loginIOSCloudUser();
								}
								xmlHttpACS = null;
							};
						
							xmlHttpACS.onerror = function(e)
							{
								xmlHttpACS = null;
							};
							
							require('/utils/Console').info(TiGlobals.appURL + 'push/push.php?deviceid='+deviceToken+'&acsuser='+deviceToken.substr(0,90)+'&acspwd='+deviceToken.substr(0,90)+'&platform=android&acsid='+user.id);
							xmlHttpACS.open('POST', TiGlobals.appURL + 'push/push.php?deviceid='+deviceToken+'&acsuser='+deviceToken.substr(0,90)+'&acspwd='+deviceToken.substr(0,90)+'&platform=android&acsid='+user.id,true);
							xmlHttpACS.send({user:Ti.App.Properties.getString('userId')});
					           
					    } else {
					        require('/utils/Console').info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					    }
					});
				}
				else
				{
					loginIOSCloudUser();
				}
				
				xmlHttp = null;
			};
		
			xmlHttp.onerror = function(e)
			{
				xmlHttp = null;
			};
			
			require('/utils/Console').info(TiGlobals.appURL + 'push/checkpush.php?deviceid='+deviceToken+'&platform=iphone');
			xmlHttp.open('POST', TiGlobals.appURL + 'push/checkpush.php?deviceid='+deviceToken+'&platform=iphone',true);
			xmlHttp.send({user:Ti.App.Properties.getString('userId')});
		}
		 
		//user login on cloud using default credential
		function loginIOSCloudUser(){
		    Cloud.Users.login({
		        login : deviceToken.substr(0,90),
				password : deviceToken.substr(0,90)
		    }, function (e) {
		    if (e.success) {
		    var user = e.users[0];
					require('/utils/Console').info("Loggin successfully");
		            iosCloudUserSubscribe();
		        } else {
		            require('/utils/Console').info("Error :"+e.message);
		        }
		    });
		}
		 
		 
		// getting device token
		
	    Titanium.Network.registerForPushNotifications({
	        types: [
	            Titanium.Network.NOTIFICATION_TYPE_BADGE,
	            Titanium.Network.NOTIFICATION_TYPE_ALERT,
	            Titanium.Network.NOTIFICATION_TYPE_SOUND
	        ],
	    	success: deviceTokenSuccess,
		    error: deviceTokenError,
		    callback: receivePush
		});
		// Process incoming push notifications
		function receivePush(e) {
		    require('/utils/Console').info('Received push: ' + JSON.stringify(e));
		}
		// Save the device token for subsequent API calls
		function deviceTokenSuccess(e) {
		    deviceToken = e.deviceToken;
		    Ti.App.Properties.setString('deviceToken',deviceToken);
		    checkDeviceTokenIOS();
		}
		
		function deviceTokenError(e) {
		    require('/utils/Console').info('Failed to register for push notifications! ' + e.error);
		}
		
		// Subscribe for push notification on cloud server
		function iosCloudUserSubscribe(){
		    Cloud.PushNotifications.subscribe({
		        channel: 'iConcerts',
		        type:'ios',
		        device_token: deviceToken
		    }, function (e) {
		        if (e.success) {
		            require('/utils/Console').info('Success :'+((e.error && e.message) || JSON.stringify(e)));
		        } else {
		            require('/utils/Console').info('Error:' + ((e.error && e.message) || JSON.stringify(e)));
		        }
		    });
		}
	}
};