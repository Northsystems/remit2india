<?php
	//Added config file for db connection
	require_once("../include/config.inc.php");
	//r_print($_SESSION);
	
	//This file use for set coutry short code, currency, country name
	require_once("../homepage.php");
	//r_print($_REQUEST);exit;
	if($_SESSION[PRE_SESSION."_POLI_SUCCESS"] == 1){
		unset($_SESSION[PRE_SESSION."_POLI_SUCCESS"]);
		header("Location:".SITE_PATH."transfer-money/");
		exit;
	}
	
	$req = rand();	
	$wsArray = array(
	"requestId"=>"$req",
	"channelId"=>TOML_CHANNELID,
	"partnerId"=>PARTNERID,
	"requestName"=>"UPDATETXNSTATUSONPAYMODERESPONSE", 
	"ipAddress"=>$iPAddress,		
	"rtrn"=>make_safe2($_REQUEST['rtrnValue']),
	"debUserId"=>"",
	"debProjectId"=>"",
	"debSenderHolder"=>"",
	"debSenderAccountNumber"=>"", 
	"debSenderBankCode"=>"",
	"debSenderCountryId"=>"",
	"debAmount"=>"",
	"debCurrencyId"=>"",
	"debUserVariable0"=>"",
	"debProjectPassword"=>"",
	"debHash"=>"",
	"debSenderBankName"=>"",
	"debSecurityCriteria"=>"",
	"debDate"=>"",
	"debTransactionId"=>"",
	"payMode"=>"POLI",
	"tranToken"=>make_safe2($_REQUEST['token']),
	);		
	$WS_FROM = "TOML";
	//r_print($wsArray);//exit;
	include("../include/curl_caller.php");
	$utsoprReturnArray = $returnArray;
	//r_print($utsoprReturnArray);//exit;
	
	$_SESSION[PRE_SESSION."_POLI_SUCCESS"] = 1;
	
	$req = rand();
	$wsArray = array(
	"requestId"=>"$req",
	"channelId"=>TOML_CHANNELID,
	"partnerId" =>PARTNERID,
	"requestName"=>"GETTRANSACTIONDETAILS",
	"rtrn"=>$_REQUEST['rtrnValue'],
	//"rtrn"=>"1212447067",
	"ipAddress"=>$iPAddress,
	"loginId"=>$_SESSION[PRE_SESSION."_USERID"],
	"sessionId"=>$_SESSION[PRE_SESSION."_SESSIONID"],
	);
	$WS_FROM = "TOML";
	//r_print($wsArray);//exit;
	include("../include/curl_caller.php");
	$gtdReturnArray = $returnArray;
	//r_print($gtdReturnArray);//exit;
	
	// Formula Based Calculations
	if($gtdReturnArray['crmBenefit'] == "NA" || $gtdReturnArray['crmBenefit'] == "na")
	{
		$netApplicableFee = $gtdReturnArray['rtrnDetails'][0]['custtxnFee']+$gtdReturnArray['rtrnDetails'][0]['banktxnFee']+$gtdReturnArray['rtrnDetails'][0]['parttxnFee'];
	}
	else{
		if($gtdReturnArray['crmBenefit'][0]['paNo'] == "FW"){
			$netApplicableFee = ($gtdReturnArray['rtrnDetails'][0]['custtxnFee']+$gtdReturnArray['rtrnDetails'][0]['banktxnFee']+$gtdReturnArray['rtrnDetails'][0]['parttxnFee'])-$gtdReturnArray['crmBenefit'][0]['promoAmount'];
		}
		else{
			$netApplicableFee = $gtdReturnArray['rtrnDetails'][0]['custtxnFee']+$gtdReturnArray['rtrnDetails'][0]['banktxnFee']+$gtdReturnArray['rtrnDetails'][0]['parttxnFee'];
		}
	}
	$convertedAmount = ($gtdReturnArray['rtrnDetails'][0]['sendOrgAmount']-$netApplicableFee)*$gtdReturnArray['rtrnDetails'][0]['payCustomerFxRate'];
	$serviceTax = $gtdReturnArray['rtrnDetails'][0]['servTaxAmt']+$gtdReturnArray['rtrnDetails'][0]['highEduCessTaxAmt']+$gtdReturnArray['rtrnDetails'][0]['eduCessTaxAmt'];
	// Formula Based Calculations
	
	// Identify isFRT Paymode
	$sourceCountry = $_SESSION[PRE_SESSION.'_CORRIDOR']."-".$_SESSION[PRE_SESSION.'_CURRENCY'];
	$destinationCountry = $_SESSION[PRE_SESSION.'_DEST']."-".$_SESSION[PRE_SESSION.'_DEST_CURRENCY'];
	
	$req = rand();	
	$wsArray = array(
	"requestId"=>"$req",
	"channelId"=>TOML_CHANNELID,
	"partnerId"=>PARTNERID,
	"requestName"=>"GETCORRIDORDTL",
    "ipAddress"=>$iPAddress,
 	"corridorRequestType"=>"2", 
 	"origCtyCurCode"=>"$sourceCountry", 
 	"destCtyCurCode"=>"$destinationCountry"
	);
	
	//r_print($wsArray);
	$WS_FROM = "TOML";
	
	include("../include/curl_caller.php");
    $corridorReturnArray = $returnArray;
	//r_print($corridorReturnArray);exit;
	$isFRT = "Y";
	
	$paymodeCode = $gtdReturnArray['rtrnDetails'][0]['paymodeCode'];
	foreach($corridorReturnArray[corridorData][0]['origCtyCurData'][0]['paymodeData'] as $corridorPaymode)
    {
    	if($corridorPaymode['paymodeCode'] == $paymodeCode)
		{
			$isFRT = $corridorPaymode['isFRTPaymode'];
		}
	}
	// Identify isFRT Paymode
	
	$meta_title = "Transfer Funds to India | Transfer Money Online to India | Remit2India";
	$meta_keywords = "";
	$meta_description = "";
	$canonical = SITE_PATH."transfer-money/poli/success/";
	$imagepath = SITE_PATH."images/logo.png";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../templates/meta-data.php") ?>	
<?php include_once("../templates/master-css.php") ?>
<link rel="stylesheet" type="text/css" href="<?=SITE_PATH?>css/transfer-money.css">
<?php include_once("../templates/master-js.php") ?>
</head>
<body>
<!--Main -->
<main>
<!--header-->
<header>
<?php include_once("../templates/header.php");?>
</header>
<!--section-->
<section id="section">
	<h1 class="title_mainTitle">
		Confirmation
	</h1>
	<div class="c_contant">
		<div class="rdTitle"><? if($gtdReturnArray['responseFlag'] == "F"){ echo $gtdReturnArray['message'];}else{echo "Your transaction is confirmed!";}?></div>
		<? /*if($gtdReturnArray['responseFlag'] == "F"){?><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p><?}*/?>
		<? if($gtdReturnArray['responseFlag'] != "F"){?><p>Thank you <?=$_SESSION[PRE_SESSION."_FNAME"]?> for choosing Remit2India for sending money to India. Remittance Transaction Reference Number (RTRN) has to be provided for each transaction and use the same for all communications with us. An email confirmation will be sent to <?=strtolower($_SESSION[PRE_SESSION."_USERID"]);?>.</p><?}?>
	</div>
	<div <? if($gtdReturnArray['responseFlag'] == "F"){?>class="hideDiv"<?}?>>
		<div class="w_Title borderTop">
			Provisional Reciept
		</div>	
		<div class="black_bg_holder">
			<div class="b_Title">Payment details</div>
		</div>
		<? if($gtdReturnArray['rtrnDetails'][0]['rtrn']){?>
		<div class="transferAmount">
			<div class="trTitle">RTRN No.</div>
			<div class="innerUsd"><?=$gtdReturnArray['rtrnDetails'][0]['rtrn']?></div>
		</div>
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['sendOrgAmount']){?>
		<div class="transferAmount">
			<div class="trTitle">Transfer Amount</div>
			<div class="innerUsd"><?=$_SESSION[PRE_SESSION.'_CURRENCY']?> <?=truncate_decimals($gtdReturnArray['rtrnDetails'][0]['sendOrgAmount'],0)?></div>
		</div>	
		<?}
		if($netApplicableFee){?>
		<div class="transferAmount">
			<div class="trTitle">Net Applicable Fee</div>
			<div class="innerUsd"><?=$_SESSION[PRE_SESSION.'_CURRENCY']?> <?=truncate_decimals($netApplicableFee,1)?></div>
		</div>	
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['sendOrgAmount']){?>
		<div class="gray_bg_holder">
			<div class="g_Title">Amount to be converted</div>
			<div class="rightUsd"><?=$_SESSION[PRE_SESSION.'_CURRENCY']?> <?=truncate_decimals((truncate_decimals($gtdReturnArray['rtrnDetails'][0]['sendOrgAmount'],0)-truncate_decimals($netApplicableFee,1)),0)?></div>
	 	</div>
	 	<?}
	 	if($gtdReturnArray['rtrnDetails'][0]['netrate']){?>
		<div class="transferAmount">
			<div class="trTitle">Exchange Rate</div>
			<div class="innerUsd">1 <?=$_SESSION[PRE_SESSION.'_CURRENCY']?> = <?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($gtdReturnArray['rtrnDetails'][0]['netrate'],2)?></div>
		</div>
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['payCustomerFxRate']){?>
		<div class="transferAmount">
			<div class="trTitle">Exchange Rate with Benefit</div>
			<div class="innerUsd">1 <?=$_SESSION[PRE_SESSION.'_CURRENCY']?> = <?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($gtdReturnArray['rtrnDetails'][0]['payCustomerFxRate'],2)?></div>
		</div>
		<?}
		if($convertedAmount){?>
		<div class="transferAmount">
			<div class="trTitle">Converted Amount</div>
			<div class="innerUsd"><?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($convertedAmount,2)?> </div>
		</div>	
		<?}
		if($serviceTax && $isFRT == "Y"){
		?>	
		<div class="transferAmount">
			<div class="trTitle">Service Tax</div>
			<div class="innerUsd"><? if($isFRT == "Y"){?>(-) <?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($serviceTax,2)?><?}else if($isFRT == "N"){?>Service tax shall be applied on the day of conversion<?}?></div>
		</div>
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['totalSbcTax'] && $isFRT == "Y"){
		?>	
		<div class="transferAmount">
			<div class="trTitle">Swachh Bharat Cess</div>
			<div class="innerUsd">(-) <?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($gtdReturnArray['rtrnDetails'][0]['totalSbcTax'],2)?></div>
		</div>
		<?}
		if($isFRT == "Y"){
		?>
		<div class="gray_bg_holder">
			<div class="g_Title">Amount to Recipient</div>
			<div class="rightUsd"><?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($gtdReturnArray['rtrnDetails'][0]['payAmount'],0)?></div>
	 	</div>
	 	<?}
		else if($isFRT == "N"){
		?>
		<div class="gray_bg_holder">
			<div class="g_Title">Amount to Recipient</div>
			<div class="rightUsd"><?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($gtdReturnArray['rtrnDetails'][0]['recvDestAmt'],0)?></div>
	 	</div>
	 	<?}
	 	if($convertedAmount && $isFRT == "N"){
		?>
		<div class="gray_bg_holder">
			<div class="g_Title">Indicative Destination Amount</div>
			<div class="rightUsd"><?=$_SESSION[PRE_SESSION.'_DEST_CURRENCY']?> <?=truncate_decimals($convertedAmount,0)?></div>
	 	</div>
	 	<?}?>
		<!-- <div class="w_Title">
			Summary of confirmation
			<div class="arrowIcon"></div>
		</div> -->
		<?if($gtdReturnArray['rtrnDetails'][0]['recvFirstName'] || $gtdReturnArray['rtrnDetails'][0]['recvLastName']){?>
		<div class="b_row">
			<div class="b_gryTitle">Remittance transfer request received for</div>
			<div class="b_wTitle"><?=$gtdReturnArray['rtrnDetails'][0]['recvFirstName']." ".$gtdReturnArray['rtrnDetails'][0]['recvLastName']?></div>
		</div>
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['paymentDeliveryMode']){?>	
		<div class="b_row">
			<div class="b_gryTitle">Delivery Mode</div>
			<div class="b_wTitle"><?=$gtdReturnArray['rtrnDetails'][0]['paymentDeliveryMode']?></div>
		</div>	
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['fircPurpose']){?>
		<div class="b_row">
			<div class="b_gryTitle">Purpose of Remittance</div>
			<div class="b_wTitle"><?=$gtdReturnArray['rtrnDetails'][0]['fircPurpose']?></div>
		</div>	
		<?}
		if($gtdReturnArray['rtrnDetails'][0]['paymodeCode']){?>
		<div class="b_row">
			<div class="b_gryTitle">Sending option selected</div>
			<!-- <div class="b_wTitle"><?=$gtdReturnArray['paymodeData'][0]['paymodeDesc']?></div> -->
			<div class="b_wTitle"><?=$gtdReturnArray['rtrnDetails'][0]['paymodeCode']?></div>
		</div>
		<?}?>
		<? if($_SESSION[PRE_SESSION.'_CORRIDOR'] == "UK"){?>
		<div class="gary_contant">
			<p>Our Nostro account details have changed from Axis Bank to Karnataka Bank. Kindly note the same before initiating the fund transfers.</p>
		</div>
		<?}
		if($_SESSION[PRE_SESSION.'_ZONE'] == "EUROPE" || $_SESSION[PRE_SESSION.'_ZONE'] == "Europe" || $_SESSION[PRE_SESSION.'_ZONE'] == "europe"){?>
		<div class="gary_contant">
			<p>Our Correspondent Bank details have changed. Kindly note the same before initiating the fund transfers.</p>
		</div>
		<?}?>
		<div class="termsconditionsHolder">
			<div class="button_getstart">
				<a href="<?=SITE_PATH?>transfer-money/" class="submit_button">Book Another Transaction</a>
			</div>
		</div>	
	</div>
</section>
<!-- pop terms of remit2India -->
<!--footer-->
<footer>
<?php include_once("../templates/footer.php");?>	
</footer>	
</main>
<!--End main-->
<script src="<?=SITE_PATH?>js/booktxn.js"></script>
</body>
</html>
