<?php
	//Added config file for db connection
	require_once("../include/config.inc.php");
	//r_print($_SESSION);
	
	//This file use for set coutry short code, currency, country name
	require_once("../homepage.php");
	
	if($_SESSION[PRE_SESSION."_POLI_FAILURE"] == 1){
		unset($_SESSION[PRE_SESSION."_POLI_FAILURE"]);
		header("Location:".SITE_PATH."transfer-money/");
		exit;
	}
	
	//r_print($_REQUEST);
	/*
	$req = rand();	
	$wsArray = array(
	"requestId"=>"$req",
	"channelId"=>TOML_CHANNELID,
	"partnerId"=>PARTNERID,
	"requestName"=>"UPDATETXNSTATUSONPAYMODERESPONSE", 
	"ipAddress"=>$iPAddress,		
	"rtrn"=>$_REQUEST['rtrnValue'],
	"debUserId"=>"",
	"debProjectId"=>"",
	"debSenderHolder"=>"",
	"debSenderAccountNumber"=>"", 
	"debSenderBankCode"=>"",
	"debSenderCountryId"=>"",
	"debAmount"=>"",
	"debCurrencyId"=>"",
	"debUserVariable0"=>"",
	"debProjectPassword"=>"",
	"debHash"=>"",
	"debSenderBankName"=>"",
	"debSecurityCriteria"=>"",
	"debDate"=>"",
	"debTransactionId"=>"",
	"payMode"=>"POLI",
	"tranToken"=>$_REQUEST['token'],
	);		
	$WS_FROM = "TOML";
	//r_print($wsArray);//exit;
	include("../include/curl_caller.php");
	$utsoprReturnArray = $returnArray;
	//r_print($utsoprReturnArray);//exit;
	*/
	$_SESSION[PRE_SESSION."_POLI_FAILURE"] = 1;
	
	$meta_title = "Transfer Funds to India | Transfer Money Online to India | Remit2India";
	$meta_keywords = "";
	$meta_description = "";
	$canonical = SITE_PATH."transfer-money/poli/failure/";
	$imagepath = SITE_PATH."images/logo.png";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../templates/meta-data.php") ?>	
<?php include_once("../templates/master-css.php") ?>
<link rel="stylesheet" type="text/css" href="<?=SITE_PATH?>css/transfer-money.css">
<?php include_once("../templates/master-js.php") ?>
</head>
<body>
<!--Main -->
<main>
<!--header-->
<header>
<?php include_once("../templates/header.php");?>
</header>
<!--section-->
<section id="section">
	<h1 class="title_mainTitle">
		Transaction Error
	</h1>
	<div id="error_div">
	  	<div class="c_spacer"><!--spacer--></div>
	  	<div class="error" align="center" id="err_msg">Your transaction has been unsuccessful / aborted</div>
	  	<div class="c_spacer"><!--spacer--></div>
  	</div>	
</section>
<footer>
<?php include_once("../templates/footer.php");?>	
</footer>	
</main>
</body>
</html>