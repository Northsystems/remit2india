exports.TxnTrackerDetailsModal = function(rtrn) 
{
	require('/lib/analytics').GATrackScreen('Transaction Details');
	
	var _obj = {
		style : require('/styles/my_account/TxnTrackerDetails').TxnTrackerDetails,
		winTxnTrackerDetailsModal : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tblTxnDetails : null
		
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winTxnTrackerDetailsModal = Ti.UI.createWindow(_obj.style.winTxnTrackerDetailsModal);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winTxnTrackerDetailsModal);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Remittance Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);

	_obj.globalView.add(_obj.mainView);
	_obj.winTxnTrackerDetailsModal.add(_obj.globalView);
	_obj.winTxnTrackerDetailsModal.open();
	
	
	// GETTRANSACTIONDETAILS
	
	function txntrackerdetails()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 10000000) + 10000) +'",'+
				'"requestName":"GETTRANSACTIONDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rtrn":"'+rtrn+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			
			require('/utils/Console').info('Result ======== ' + e.result);
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === 'S')
			{
				_obj.tblTxnDetails = Ti.UI.createTableView(_obj.style.tableView);
				
				if(origSplit[0] =='US')
				{
                    if(e.result.rtrnDetails[0].paymodeCode === 'WIRE')
				     {
					     var rowCount = 14;           //13 to 14 changed by Sanjivani on 17Nov for bug.570
				     }
				    else
				     {
					      var rowCount = 13;          //12 to 13 changed by Sanjivani on 17Nov for bug.570
				     }
				}
				else
				{
					if(e.result.rtrnDetails[0].paymodeCode === 'WIRE')
					{
						var rowCount = 13;
					}
					else
					{
						var rowCount = 12;
					}
				}
				for(var i=0; i<=rowCount; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKey = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					var lblKey1 = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color:TiFonts.FontStyle('redFont'),
					});
					
					var lblValue = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var btnCancel =Ti.UI.createButton(_obj.style.btnCancel);  //Added by Sanjivani on 17Nov for bug.570
					
					if(origSplit[0] =='US')
					{
					switch(i)
					{
						case 0:
							lblKey.text = 'RTRN No.';
							lblValue.text = e.result.rtrnDetails[0].rtrn;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 1:
							lblKey.text = 'Date of Instruction';
							lblValue.text = e.result.rtrnDetails[0].instructionDate;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 2:
							lblKey.text = 'Remitted Amount';
							lblValue.text = origSplit[1] + ' ' + e.result.rtrnDetails[0].sendOrgAmount;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 3:
							lblKey.text = 'Mode of Payment';
							lblValue.text = e.result.rtrnDetails[0].paymodeCode;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;

						case 4:
							lblKey.text = 'Actual Destination Amount';
							lblValue.text = e.result.rtrnDetails[0].destCurrency + ' '+ e.result.rtrnDetails[0].netRecvDestAmt;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 5:
							lblKey.text = 'Receiver\'s Nick Name';
							lblValue.text = e.result.rtrnDetails[0].recvNickName;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 6:
							lblKey.text = 'Receiver\'s Name';
							lblValue.text = e.result.rtrnDetails[0].recvFirstName + ' ' + e.result.rtrnDetails[0].recvLastName;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 7:
							lblKey.text = 'Receiver\'s Address';
							lblValue.text = e.result.rtrnDetails[0].recvAddress + ',\n' + e.result.rtrnDetails[0].recvCity + '-' + e.result.rtrnDetails[0].recvPincode + '\n' + e.result.rtrnDetails[0].recvState + ', ' + e.result.rtrnDetails[0].recvCountry; 
							
							lblValue.height = Ti.UI.SIZE;
							lblValue.bottom = 10;
							row.height = Ti.UI.SIZE;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 8:
							lblKey.text = 'Receiver\'s Phone';
							lblValue.text = e.result.rtrnDetails[0].recvResPhone;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 9:
							lblKey.text = 'Delivery Mode';
							if(e.result.rtrnDetails[0].paymentDeliveryMode !== '')
							{
								lblValue.text = e.result.rtrnDetails[0].paymentDeliveryMode === 'DC' ? 'Account Credit' : 'Demand Draft';
							}
							else
							{
								lblValue.text = 'N.A.';
							}
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 10:
							lblKey.text = 'Delivery Details';
							lblValue.text = e.result.rtrnDetails[0].recvBankName + '\na/c no - ' + e.result.rtrnDetails[0].recvAccNumber; 
							
							lblValue.height = Ti.UI.SIZE;
							lblValue.bottom = 10;
							row.height = Ti.UI.SIZE;
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 11:
							lblKey.text = 'Status of Remittance Request';
							lblValue.text = e.result.rtrnDetails[0].statusDescription; 
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 12:
							lblKey.text = 'Remittance Sub-Status';
							lblValue.text = e.result.rtrnDetails[0].subStatusDescription !== '' ? e.result.rtrnDetails[0].subStatusDescription : 'N.A.'; 
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 13:
							lblKey.text = 'Remarks';
							lblValue.text = e.result.rtrnDetails[0].statusRemarks !== '' ? e.result.rtrnDetails[0].statusRemarks : 'N.A.'; 
							
							row.add(lblKey);
							row.add(lblValue);
							_obj.tblTxnDetails.appendRow(row);
						break;
						
						case 14:
							if(e.result.rtrnDetails[0].paymodeCode === 'WIRE'){
								Ti.API.info("In if");
							lblKey.height = Ti.UI.SIZE;
							lblKey.width = Ti.UI.SIZE;
							lblKey.bottom = 10;
							
							e.result.nostroDetails !== 'NA' ? lblKey1.text = 'Click here to view the bank details for transfer of funds' : lblKey.text = 'N.A.';
							
							row.rw = 1;
							row.nostroDetails = e.result.nostroDetails;
							row.add(lblKey);
							row.add(lblKey1);
							_obj.tblTxnDetails.appendRow(row);
							/*btnCancel.title = 'Cancel';
							row.add(btnCancel);
						    _obj.tblTxnDetails.appendRow(row);*/
						  }
							else{
								Ti.API.info("In Else");
								btnCancel.title = 'Cancel';
								row.add(btnCancel);
							    _obj.tblTxnDetails.appendRow(row);
							}
						break;
						
						case 15:                            //Added by Sanjivani on 17Nov for bug.570
							btnCancel.title = 'Cancel';
							row.add(btnCancel);
							_obj.tblTxnDetails.appendRow(row);
					  
							
							
					} //first switch ends here
				}//end of if
					else{
						switch(i)
						{
							case 0:
								lblKey.text = 'RTRN No.';
								lblValue.text = e.result.rtrnDetails[0].rtrn;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 1:
								lblKey.text = 'Date of Instruction';
								lblValue.text = e.result.rtrnDetails[0].instructionDate;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 2:
								lblKey.text = 'Remitted Amount';
								lblValue.text = origSplit[1] + ' ' + e.result.rtrnDetails[0].sendOrgAmount;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 3:
								lblKey.text = 'Mode of Payment';
								lblValue.text = e.result.rtrnDetails[0].paymodeCode;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 4:
								lblKey.text = 'Receiver\'s Nick Name';
								lblValue.text = e.result.rtrnDetails[0].recvNickName;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 5:
								lblKey.text = 'Receiver\'s Name';
								lblValue.text = e.result.rtrnDetails[0].recvFirstName + ' ' + e.result.rtrnDetails[0].recvLastName;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 6:
								lblKey.text = 'Receiver\'s Address';
								lblValue.text = e.result.rtrnDetails[0].recvAddress + ',\n' + e.result.rtrnDetails[0].recvCity + '-' + e.result.rtrnDetails[0].recvPincode + '\n' + e.result.rtrnDetails[0].recvState + ', ' + e.result.rtrnDetails[0].recvCountry; 
								
								lblValue.height = Ti.UI.SIZE;
								lblValue.bottom = 10;
								row.height = Ti.UI.SIZE;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 7:
								lblKey.text = 'Receiver\'s Phone';
								lblValue.text = e.result.rtrnDetails[0].recvResPhone;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 8:
								lblKey.text = 'Delivery Mode';
								if(e.result.rtrnDetails[0].paymentDeliveryMode !== '')
								{
									lblValue.text = e.result.rtrnDetails[0].paymentDeliveryMode === 'DC' ? 'Account Credit' : 'Demand Draft';
								}
								else
								{
									lblValue.text = 'N.A.';
								}
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 9:
								lblKey.text = 'Delivery Details';
								lblValue.text = e.result.rtrnDetails[0].recvBankName + '\na/c no - ' + e.result.rtrnDetails[0].recvAccNumber; 
								
								lblValue.height = Ti.UI.SIZE;
								lblValue.bottom = 10;
								row.height = Ti.UI.SIZE;
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 10:
								lblKey.text = 'Status of Remittance Request';
								lblValue.text = e.result.rtrnDetails[0].statusDescription; 
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 11:
								lblKey.text = 'Remittance Sub-Status';
								lblValue.text = e.result.rtrnDetails[0].subStatusDescription !== '' ? e.result.rtrnDetails[0].subStatusDescription : 'N.A.'; 
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 12:
								lblKey.text = 'Remarks';
								lblValue.text = e.result.rtrnDetails[0].statusRemarks !== '' ? e.result.rtrnDetails[0].statusRemarks : 'N.A.'; 
								
								row.add(lblKey);
								row.add(lblValue);
								_obj.tblTxnDetails.appendRow(row);
							break;
							
							case 13:
								lblKey.height = Ti.UI.SIZE;
								lblKey.width = Ti.UI.SIZE;
								lblKey.bottom = 10;
								
								e.result.nostroDetails !== 'NA' ? lblKey1.text = 'Click here to view the bank details for transfer of funds' : lblKey.text = 'N.A.';
								
								row.rw = 1;
								row.nostroDetails = e.result.nostroDetails;
								row.add(lblKey);
								row.add(lblKey1);
								_obj.tblTxnDetails.appendRow(row);
							break;
						} //switch end
					}
				}
				//_obj.mainView.add(_obj.tblTxnDetails);
				_obj.mainView.add(_obj.tblTxnDetails);
				
				_obj.tblTxnDetails.addEventListener('click', function(e) {
					Ti.API.info('Clicked index1: ' + e.index);
					try{
						if(e.row.rw === 1)
						{
							if(e.row.nostroDetails !== 'NA')
							{
								require('/js/my_account/DomesticWireTransferDetailsModal').DomesticWireTransferDetailsModal(rtrn,e.row.nostroDetails[0]);
							}
						}
					}//end of try
					catch(e){}
				});
						
							//Sanjivani1
							// Cancel transaction
							var myMesg = null;
						     btnCancel.addEventListener('click', function(evt) { 
							        	   var alertDialog = require('/utils/AlertDialog').showAlert('', 'Once you click on OK, the transaction will be deleted.', [L('btn_ok'), L('btn_cancel')]);
											alertDialog.show();

											alertDialog.addEventListener('click', function(evt) {
												alertDialog.hide();
												if (evt.index === 0 || evt.index === "0") {
													activityIndicator.showIndicator();
										var xhr = require('/utils/XHR');
										
										xhr.call({
											url : TiGlobals.appURLTOML,
											get : '',
											post : '{' +
												'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
												'"requestName":"CANCELTRANSACTION",'+
												'"partnerId":"'+TiGlobals.partnerId+'",'+
												'"channelId":"'+TiGlobals.channelId+'",'+
												'"ipAddress":"'+TiGlobals.ipAddress+'",'+
												'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
												'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
												'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
												//'"rtrn":"'+e.row.rtrn+'"'+
												'"rtrn":"'+rtrn+'"'+
												'}',
											success : xhrSuccess,
											error : xhrError,
											contentType : 'application/json',
											timeout : 10000
										});
										
										function xhrSuccess(e) {
											require('/utils/Console').info('Result ======== ' + e.result);
											activityIndicator.hideIndicator();
											
											if(e.result.responseFlag === "S")
											{
												myMesg = e.result.message;
												Ti.API.info("Cancel transaction message is" +myMesg);
												if(TiGlobals.osname === 'android')
												{
													
													destroy_txntrackerdetails();
													require('/js/my_account/TransactionTrackerModal').TransactionTrackerModal();//
													require('/utils/AlertDialog').toast(e.result.message);
													/*setTimeout(function(){
														require('/utils/AlertDialog').toast(e.result.message);
														}, 3000);*/
													
												}
												else
												{
													
													destroy_txntrackerdetails();
													require('/js/my_account/TransactionTrackerModal').TransactionTrackerModal();//
													require('/utils/AlertDialog').iOSToast(e.result.message);
												}
												
												//trackTxn();
											}
											else
											{
												if(e.result.displayBankMessage === L('invalid_session') || e.result.message === 'Invalid Session')
												{
													require('/lib/session').session();
												destroy_txntrackerdetails();
												}
												else
												{
													require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
													destroy_txntrackerdetails();
												}
											}
											
											xhr = null;
										}
								
										function xhrError(e) {
											activityIndicator.hideIndicator();
											require('/utils/Network').Network();
											xhr = null;
										}
									}
								});
							//}//end of cancel if
							});
							//}
							//Sanjivani 1 end
							
						//}//end of if //already there
				
			}  //end if of success response
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_txntrackerdetails();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	txntrackerdetails();
	
	function destroy_txntrackerdetails()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove transaction tracker details start ##############');
			
			_obj.winTxnTrackerDetailsModal.close();
			require('/utils/RemoveViews').removeViews(_obj.winTxnTrackerDetailsModal);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_transactiontracker',destroy_transactiontracker);
			require('/utils/Console').info('############## Remove transaction tracker details end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}

	
	_obj.imgClose.addEventListener('click',function(e){
		//require('/utils/RemoveViews').removeAllScrollableViews();
	
		destroy_txntrackerdetails();
		require('/js/my_account/TransactionTrackerModal').TransactionTrackerModal();//
		
		
	});
	
	_obj.winTxnTrackerDetailsModal.addEventListener('androidback',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_txntrackerdetails();
		require('/js/my_account/TransactionTrackerModal').TransactionTrackerModal(); //by sanjivani on 22nov16
	});
	
	function destroy_txntrackerdetails()
	{
		try{
			
			require('/utils/Console').info('############## Remove TxnTrackerDetails start ##############');
			
			_obj.winTxnTrackerDetailsModal.close();
			require('/utils/RemoveViews').removeViews(_obj.winTxnTrackerDetailsModal);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_txntrackerdetails',destroy_txntrackerdetails);
			require('/utils/Console').info('############## Remove TxnTrackerDetails end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_txntrackerdetails', destroy_txntrackerdetails);
}; // TxnTrackerDetailsModal()
