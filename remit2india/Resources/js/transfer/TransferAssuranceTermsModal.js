exports.TransferAssuranceTermsModal = function()
{
	require('/lib/analytics').GATrackScreen('Transfer Assurance Terms');
	
	var _obj = {
		style : require('/styles/transfer/TransferAssuranceMore').TransferAssuranceMore,
		winTransferAssuranceTerms : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblSectionHead : null,
		sectionHeaderBorder : null,
		webView : null,
		currCode : null,
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winTransferAssuranceTerms = Ti.UI.createWindow(_obj.style.winTransferAssuranceMore);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winTransferAssuranceTerms);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Terms & Conditions';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'Remit2India Transaction\nAssurance Program';
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder); 
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'terms-condition.php?corridor='+_obj.currCode[0];
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	_obj.winTransferAssuranceTerms.add(_obj.globalView);
	_obj.winTransferAssuranceTerms.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_transferterms();
	});
	
	_obj.winTransferAssuranceTerms.addEventListener('androidback', function(){
		destroy_transferterms();
	});
	
	function destroy_transferterms()
	{
		try{
			
			require('/utils/Console').info('############## Remove transfer terms start ##############');
			
			_obj.winTransferAssuranceTerms.close();
			require('/utils/RemoveViews').removeViews(_obj.winTransferAssuranceTerms);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_transferterms',destroy_transferterms);
			require('/utils/Console').info('############## Remove transfer terms end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_transferterms', destroy_transferterms);
}; // TransferAssuranceTermsModal()