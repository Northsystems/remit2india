exports.calendar = function (title, description, scheduleStartTime, scheduleEndTime, scheduleDate)
{
	require('/utils/Console').info(title+ ' ---- ' +scheduleStartTime+ ' ---- ' +scheduleEndTime+ ' ---- ' +scheduleDate);
    var scheduleStartSplit= scheduleStartTime.split(':');
	var scheduleEndSplit = scheduleEndTime.split(':');
	var scheduleDateSplit = scheduleDate.split('-');
	
	// Create the event 
	var eventBegins = new Date(parseInt(scheduleDateSplit[0]), parseInt(scheduleDateSplit[1])-1, parseInt(scheduleDateSplit[2]), parseInt(scheduleStartSplit[0]), parseInt(scheduleStartSplit[1]), parseInt(scheduleStartSplit[2]));
	var eventEnds = new Date(parseInt(scheduleDateSplit[0]), parseInt(scheduleDateSplit[1])-1, parseInt(scheduleDateSplit[2]), parseInt(scheduleEndSplit[0]), parseInt(scheduleEndSplit[1]), parseInt(scheduleEndSplit[2]));
	
	if(TiGlobals.osname === 'android')
	{
		var CALENDAR_TO_USE = 1;
		var calendar = Ti.Calendar.getCalendarById(CALENDAR_TO_USE);
		
		var details = {
		    title: title,
		    description: description,
		    begin: eventBegins,
		    end: eventEnds
		};
		
		var event = calendar.createEvent(details);
		
		// Now add a reminder via alert for 10 minutes before the event.
		var reminderDetails = {
		    minutes: 10,
		    method: Ti.Calendar.METHOD_ALERT
		};
		event.createReminder(reminderDetails);
		require('/utils/AlertDialog').showAlert('','Show reminder has been added to the calendar',['OK']).show();
	} 
	else
	{
		function performCalendarWriteFunctions()
		{
		    var defCalendar = Ti.Calendar.defaultCalendar;
		    
		    var event = defCalendar.createEvent({
                title: title,
                notes: description,
                location: '',
                begin: eventBegins,
                end: eventEnds,
                availability: Ti.Calendar.AVAILABILITY_FREE,
                allDay: false,
            });
		    var alert = event.createAlert({
            	absoluteDate: new Date(new Date().getTime() - (1000*60*20))
            });
		    event.alerts = alert;
		    
		    require('/utils/Console').info('Time'+new Date(new Date().getTime() - (1000*60*20)));
		    
		    require('/utils/Console').info('Going to save event now');
		    event.save(Ti.Calendar.SPAN_THISEVENT);
		    require('/utils/Console').info('Done with saving event,\n Now trying to retreive it.');
		    require('/utils/Console').info(event.id);
		    require('/utils/AlertDialog').showAlert('','Show reminder has been added to the calendar',['OK']).show();
		}
		
		
		if(Ti.Calendar.eventsAuthorization === Ti.Calendar.AUTHORIZATION_AUTHORIZED) 
		{
		    performCalendarWriteFunctions();
		} 
		else 
		{
		    Ti.Calendar.requestEventsAuthorization(function(e){
	            if (e.success) {
	                performCalendarWriteFunctions();
	            } else {
	                require('/utils/AlertDialog').showAlert('','Access to calendar is not allowed',['OK']).show();
	            }
	        });
		}
	}
};