var _properties = {}, _platform;

function plus(_points, _show) {

    if (typeof _points !== 'number') {
        _points = 1;
    }

    return _trigger(_points, _show);
}

function test() {
    return _trigger();
}

function ask() {

    if (typeof _platform !== 'string') {
        _platform = Ti.Platform.name;
    }

    if (_platform === 'iPhone OS' && !exports.appleId) {

        _lookup(function(result) {

            if (!result) {
                require('/utils/Console').info('[RATE] Lookup failed.');
                return;
            }

            exports.appleId = result.trackId;

            return ask();
        });

        return;
    }

    var buttonNames = [exports.yes, exports.later, exports.never];
    var cancel = buttonNames.length - 1;

    var alertDialog = Titanium.UI.createAlertDialog({
        title: exports.title,
        message: exports.message,
        buttonNames: buttonNames,
        cancel: cancel
    });

    alertDialog.addEventListener('click', function(e) {

        if (buttonNames[e.index] === exports.yes) {
            Ti.App.Properties.setString('rate_done', Ti.App.version);

            open();

            if (exports.listener !== null) {
                exports.listener({
                    type: 'yes'
                });
            }

        } else if (buttonNames[e.index] === exports.never) {
            Ti.App.Properties.setBool('rate_never', true);

            if (exports.listener !== null) {
                exports.listener({
                    type: 'never'
                });
            }

        } else {

            if (exports.listener !== null) {
                exports.listener({
                    type: 'later'
                });
            }
        }

        return;
    });

    alertDialog.show();

    return;
}

function open() {

    if (Ti.Platform.name === 'android') {
        Ti.Platform.openURL('market://details?id=' + Ti.App.id);

    } else if (Ti.Platform.name === 'iPhone OS') {

        if (!exports.appleId) {
            _lookup(function(result) {

                if (!result) {
                    require('/utils/Console').info('[RATE] Lookup failed.');
                    return;
                }

                exports.appleId = result.trackId;
                return open();
            });

            return;
        }

        Ti.Platform.openURL('http://itunes.apple.com/app/id' + exports.appleId);
    }

    return;
}

function reset() {
    require('/utils/Console').info('[RATE] Reset.');

    Ti.App.Properties.removeProperty('rate_done');
    Ti.App.Properties.removeProperty('rate_never');
    Ti.App.Properties.removeProperty('rate_points');
    Ti.App.Properties.removeProperty('rate_asked');

    return;
}

function _trigger(_points, _show) {

    if (Ti.App.Properties.getBool('rate_never', false) === true) {
        require('/utils/Console').info('[RATE] Rating disabled by user.');
        return false;
    }

    var rate_done = Ti.App.Properties.getString('rate_done');

    if (exports.eachVersion ? (rate_done === Ti.App.version) : rate_done) {
        require('/utils/Console').info('[RATE] Rating already done.');
        return false;
    }

    var points = Ti.App.Properties.getInt('rate_points', 0);

    if (_points) {
        points = points + (_points || 1);
        require('/utils/Console').info('[RATE] Rating points changed to: ' + points);
    }

    var now = (new Date() / 1000),
        checked = Ti.App.Properties.getInt('rate_asked', 0);

    if (_show !== false) {

        if (checked === 0) {
            Ti.App.Properties.setInt('rate_asked', now);
            checked = now;
        }

        if (points < exports.pointsBetween) {
            require('/utils/Console').info('[RATE] Not enough points: ' + points + ' of ' + exports.pointsBetween);
            _show = false;

        } else if ((now - checked) < (exports.daysBetween * 86400)) {
            require('/utils/Console').info('[RATE] Not enough days' + (checked ? (': ' + Math.round((now - checked) / 86400) + ' of ' + exports.daysBetween) : ''));
            _show = false;
        }
    }

    if (_show !== false) {
        require('/utils/Console').info('[RATE] Rating triggered!');

        Ti.App.Properties.setInt('rate_points', 0);
        Ti.App.Properties.setInt('rate_asked', now);

        ask();

        return true;

    } else {
        Ti.App.Properties.setInt('rate_points', points);
    }

    return false;
}

function _lookup(_callback) {
    var xhr = Ti.Network.createHTTPClient({
        onload: function(e) {
            if (xhr.status === 200 && this.responseText) {
                try {
                    var json = JSON.parse(this.responseText);

                    if (json.resultCount === 1) {
                        _callback(json.results[0]);
                        return;

                    } else {
                        Ti.API.error('[RATE] LOOKUP ERROR ' + this.responseText);
                    }

                } catch (err) {
                    Ti.API.error('[RATE] LOOKUP ERROR ' + JSON.stringify(err));
                }
            }

            _callback();
            return;
        },
        onerror: function(e) {
            Ti.API.error('[RATE] LOOKUP ERROR ' + JSON.stringify(e.error));
            _callback();
            return;
        }
    });

    var url = 'http://itunes.apple.com/lookup?';

    if (exports.appleId) {
        url = url + 'id=' + exports.appleId;
    } else {
        url = url + 'bundleId=' + Ti.App.id;
    }

    xhr.open('GET', url);
    xhr.send();

    return;
}

exports.title = 'Rate ' + Ti.App.getName();
exports.message = 'If you enjoy using ' + Ti.App.getName() + ', would you mind taking a moment to rate it? It won\'t take more than a minute. Thanks for your support!';
exports.yes = 'Yes';
exports.later = 'Remind me later';
exports.never = 'No';

exports.appleId = null;
exports.daysBetween = 0; // 0 for onclick rating & 3 for asking after 3 days again
exports.pointsBetween = 3; // 1 for onclick rating & 3 for asking after app is visited thrice
exports.eachVersion = false;

exports.plus = plus;
exports.test = test;
exports.ask = ask;
exports.open = open;
exports.reset = reset;

exports.listener = null;