///////////////////// Analytics ///////////////////

exports.GATrackScreen = function(pg)
{
	require('/utils/Console').info('Track Screen');
	var GA = require('ti.ga');
	var tracker = GA.createTracker({
	   trackingId:TiGlobals.googleAPI,
	   useSecure:true,
	   debug:true
	});
	
	tracker.addScreenView(pg);
};

exports.GATrackEvent = function(options)
{
	require('/utils/Console').info('Track Event');
	var GA = require('ti.ga');
	var tracker = GA.createTracker({
	   trackingId:TiGlobals.googleAPI,
	   useSecure:true,
	   debug:true
	});
	
	tracker.addEvent({
        category:options.category,
        label:options.label,
        value:options.value
    });
};

exports.GATrackSocial = function(options)
{
	require('/utils/Console').info('Track Social');
	var GA = require('ti.ga');
	var tracker = GA.createTracker({
	   trackingId:TiGlobals.googleAPI,
	   useSecure:true,
	   debug:true
	});
	
	tracker.addSocialNetwork({
        network:options.network,
        action:options.action,
        target:options.target
    });
};