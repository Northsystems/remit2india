exports.Network = function()
{
	require('/utils/Console').info('Network ========== ' + Ti.Network.online);
	
	if(TiGlobals.osname === 'android')
	{
		require('/utils/AlertDialog').toast('No Internet Connection');
	}
	else
	{
		require('/utils/AlertDialog').iOSToast('No Internet Connection');
	}
};