exports.showAlert = function(title,message,btn)
{
	var customAlert = Ti.UI.createAlertDialog();
	customAlert.title = title; 
	customAlert.message = message; 
	customAlert.buttonNames = btn;
	
	return customAlert;
};

exports.toast = function(message)
{
	var toast = Ti.UI.createNotification({
	    message:message,
	    duration: Ti.UI.NOTIFICATION_DURATION_LONG
	});
	toast.show();
};

exports.iOSToast = function(message)
{
	var toast = Ti.UI.createWindow({
		bottom:100,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		backgroundColor:'transparent',
		opacity: 0.0
	});
	
	var view = Ti.UI.createView({
		height:Ti.UI.SIZE,
		left:20,
		right:20,
		width:Ti.UI.SIZE,
		backgroundColor:'#000',
    	borderRadius:20,
	});
	
	var label = Ti.UI.createLabel({
		text:message,
		top:15,
		bottom:15,
		height:Ti.UI.SIZE,
		left:15,
		right:15,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('whiteFont')
	});
	
	view.add(label);
	toast.add(view);
	
	toast.open();
	
	try{
		toast.animate({
		  opacity: 0.8,
		  duration:250
		},function(){
			setTimeout(function(){
				toast.animate({
				  opacity: 0.0,
				  duration:250
				},function(){
					toast.close();
					toast = null;
					view = null;
					label = null;
				});
			},2000);
		});
	}catch(e){}	
};