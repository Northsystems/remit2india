function ActivityIndicator(_self){
	this.viewIndicatorModal = null;
	this.indicatorView = null;
	this.activityIndicator = null;
	TiGlobals.indicatorCheck = 0; // For multiple indicators overlap * app hangs
	
	this.viewIndicatorModal = Ti.UI.createView({
		top : '0dp',
		left : '0dp',
		right : '0dp',
		bottom : '0dp',
		backgroundColor: 'transparent',
		visible:false,
		zIndex : 10000
	});

	this.indicatorView = Ti.UI.createView({
		backgroundColor : '#000',
		height : 40,
		width : 40,
		borderRadius:5
	});

	this.activityIndicator = Ti.UI.createActivityIndicator({
        style: Ti.UI.ActivityIndicatorStyle.PLAIN
    });
    
    try{
    	this.indicatorView.add(this.activityIndicator);
    	this.viewIndicatorModal.add(this.indicatorView);
	    _self.add(this.viewIndicatorModal);
	}
	catch(e){
	}
}

ActivityIndicator.prototype.showIndicator = function(){
	if(TiGlobals.indicatorCheck === 0)
	{
		this.viewIndicatorModal.show();
		this.activityIndicator.show();
		TiGlobals.indicatorCheck = 1;
	}
};
	
ActivityIndicator.prototype.hideIndicator = function ()
{
	if(TiGlobals.indicatorCheck === 1)
	{
		try{
			this.activityIndicator.hide();
			this.viewIndicatorModal.hide();
		    TiGlobals.indicatorCheck = 0;
		}catch(e){
		}
	}
};

module.exports = ActivityIndicator;
