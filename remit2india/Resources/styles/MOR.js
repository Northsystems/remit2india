/*global exports,require*/
var _ = require('/utils/Underscore');
var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.MOR = {
	winMOR : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		layout:'vertical'
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	imgBack : {
		left:0,
		image:'/images/back.png',
	    width:35,
	    height:40
	},
	headerBorder : {
		top:0,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	tableView : {
		top:0,
		left:0,
		bottom:0,
		right:0,
		scrollable:false,
		showVerticalScrollIndicator:false,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#efeff0',
		backgroundColor:'transparent',
		footerTitle:TiGlobals.osname === 'android' ? null : '' // Fix for empty rows in iOS
	},
	
	btn : _.defaults({top:20}, button),
};